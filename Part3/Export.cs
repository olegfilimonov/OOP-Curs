﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part3
{
    public class Export
    {
        public int A { get; set; }
        public int B { get; set; }
        public int C { get; set; }
        public int D { get; set; }
        public int E { get; set; }
        public int F { get; set; }

        public Export()
        {
            A = 0;
            B = 0;
            C = 0;
            D = 0;
            E = 0;
            F = 0;
        }

        public int[] GetArray()
        {
            return new[] {A,B,C,D,E,F};
        }
    }
}

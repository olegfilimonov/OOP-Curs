﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using GraphX.Controls;
using GraphX.PCL.Common.Enums;
using GraphX.PCL.Logic.Algorithms.OverlapRemoval;
using GraphX.PCL.Logic.Models;
using QuickGraph;
using Application = Microsoft.Office.Interop.Excel.Application;

namespace Part3
{
   
    public partial class Form1 : Form
    {
        private const int N = 6;
        public List<Export> Exports = new List<Export>(N);
        private ZoomControl _zoomctrl;
        private GraphAreaExample _gArea;
        private int[][] _dists = {
            new int[6],
            new int[6],
            new int[6],
            new int[6],
            new int[6],
            new int[6]
        };


        public Form1()
        {
            InitializeComponent();
            elementHost1.Child = GenerateWpfVisuals();
            _zoomctrl.ZoomToFill();
            _gArea.GenerateGraph();
        }

        private UIElement GenerateWpfVisuals()
        {
            _zoomctrl = new ZoomControl();
            ZoomControl.SetViewFinderVisibility(_zoomctrl, Visibility.Visible);
            /* ENABLES WINFORMS HOSTING MODE --- >*/
            var logic = new GXLogicCore<DataVertex, DataEdge, BidirectionalGraph<DataVertex, DataEdge>>();
            _gArea = new GraphAreaExample() { EnableWinFormsHostingMode = true, LogicCore = logic };
            logic.Graph = GenerateGraph();
            logic.DefaultLayoutAlgorithm = LayoutAlgorithmTypeEnum.LinLog;
            logic.DefaultLayoutAlgorithmParams = logic.AlgorithmFactory.CreateLayoutParameters(LayoutAlgorithmTypeEnum.LinLog);
            //((LinLogLayoutParameters)logic.DefaultLayoutAlgorithmParams). = 100;
            logic.DefaultOverlapRemovalAlgorithm = OverlapRemovalAlgorithmTypeEnum.FSA;
            logic.DefaultOverlapRemovalAlgorithmParams = logic.AlgorithmFactory.CreateOverlapRemovalParameters(OverlapRemovalAlgorithmTypeEnum.FSA);
            ((OverlapRemovalParameters)logic.DefaultOverlapRemovalAlgorithmParams).HorizontalGap = 50;
            ((OverlapRemovalParameters)logic.DefaultOverlapRemovalAlgorithmParams).VerticalGap = 50;
            logic.DefaultEdgeRoutingAlgorithm = EdgeRoutingAlgorithmTypeEnum.None;
            logic.AsyncAlgorithmCompute = false;
            _zoomctrl.Content = _gArea;
            _gArea.RelayoutFinished += gArea_RelayoutFinished;


            var myResourceDictionary = new ResourceDictionary { };
            _zoomctrl.Resources.MergedDictionaries.Add(myResourceDictionary);

            return _zoomctrl;
        }

        void gArea_RelayoutFinished(object sender, EventArgs e)
        {
            _zoomctrl.ZoomToFill();
        }

        private GraphExample GenerateGraph()
        {
            //FOR DETAILED EXPLANATION please see SimpleGraph example project
            var dataGraph = new GraphExample();
            for (int i = 0; i < 6; i++)
            {
                var dataVertex = new DataVertex( ((char)('A' + i)).ToString());
                dataGraph.AddVertex(dataVertex);
            }

            var vlist = dataGraph.Vertices.ToList();
            //Then create two edges optionaly defining Text property to show who are connected
            for (var i = 0; i < 6; i++)
            {
                for (var j = 0; j < 6; j++)
                {
                    if (_dists[i][j] > 0)
                    {
                        dataGraph.AddEdge(new DataEdge(vlist[i], vlist[j], _dists[i][j]));
                    }
                }
            }
            return dataGraph;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            for (var i = 0; i < N; i++)
            {
                var export = new Export();
                Exports.Add(export);
                bindingSource1.Add(export);
            }

        }

        private void ToCsV()
        {
            if (dataGridView1.Rows.Count <= 0) return;
            var xcelApp = new Application();
            xcelApp.Application.Workbooks.Add(Type.Missing);

            for (var i = 1; i < dataGridView1.Columns.Count + 1; i++)
            {
                xcelApp.Cells[1, i] = dataGridView1.Columns[i - 1].HeaderText;
            }

            for (var i = 0; i < dataGridView1.Rows.Count; i++)
            {
                for (var j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    xcelApp.Cells[i + 2, j + 1] = dataGridView1.Rows[i].Cells[j].Value.ToString().Replace(',', '.');
                }
            }
            xcelApp.Columns.AutoFit();
            xcelApp.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ToCsV();
        }



        private void button1_Click(object sender, EventArgs e)
        {
            var antColony = new AntColony();
            _dists = new[]
            {
                Exports[0].GetArray(),
                Exports[1].GetArray(),
                Exports[2].GetArray(),
                Exports[3].GetArray(),
                Exports[4].GetArray(),
                Exports[5].GetArray()
            };
            for (var i = 0; i < 6; i++)
            {
                _dists[i][i]=0;
            }

            //var distsCopy = new[]
            //{
            //    new int[6],
            //    new int[6],
            //    new int[6],
            //    new int[6],
            //    new int[6],
            //    new int[6]
            //};
            //for (var j = 0; j < 6; j++)
            //{
            //    dists[j].CopyTo(distsCopy[j], 0);
            //}

            //var start = 0; //A
            //var accessible = new List<int>();

            //var correctInput = true;

            //for (var i = 0; i < 6; i++)
            //{
            //    CheckForAccessibility(ref accessible, ref distsCopy, i);

            //    if (accessible.Union(new List<int> {0, 1, 2, 3, 4, 5}).Count() == 5)
            //    {
            //        accessible.Clear();
            //        for (var j = 0; j < 6; j++)
            //        {
            //            dists[j].CopyTo(distsCopy[j], 0);
            //        }
            //    }
            //    else
            //    {
            //        correctInput = false;
            //        MessageBox.Show("Некорректный ввод: граф не связный. (Из вершины "+(char)('A' + i)+" нельзя попасть во все остальные)");
            //        break;
            //    }
            //}

            //if (!correctInput) return;

            elementHost1.Child = GenerateWpfVisuals();
            _zoomctrl.ZoomToFill();
            _gArea.GenerateGraph();
            _gArea.SetVerticesDrag(true, true);
            _zoomctrl.ZoomToFill();

            antColony.Execute(_dists);
            dataGridView1.Update();

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        //private static void CheckForAccessibility(ref List<int> accesible,ref int[][] dists, int i)
        //{
        //    for (var j = 0; j < 6; j++)
        //    {
        //        if (dists[i][j] <= 0) continue;
        //        dists[i][j] = 0;
        //        accesible.Add(j);
        //        CheckForAccessibility(ref accesible, ref dists, j);
        //    }
        //}
    }
}

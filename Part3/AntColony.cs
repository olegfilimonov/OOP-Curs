﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Part3
{

    public class AntColony
    {
        // ПРИВАТНЫЕ ПОЛЯ

        // Генерируем рандом и используем текущее время как семечку
        private static readonly Random Random = new Random(DateTime.Now.Millisecond);
        private const int NoPath = int.MaxValue;
        private int[][] _dists;
        private int[][] _ants;
        private int[] _bestTrail;
        private double[][] _pheromones;

        // СВОЙСТВА

        // Параметры формул
        public int Alpha { get; } = 3;
        public int Beta { get; } = 2;
        // Фактор уменьшения феромонов
        public double Rho { get; } = 0.01;
        // Фактор увеличения феромонов
        public double Q { get; } = 2.0;

        public int NumCities { get; } = 6;
        public int NumAnts { get; } = 4;
        public int MaxTime { get; } = 1000;

        // КОНСТРУКТОРЫ

        public AntColony()
        {
        }

        public AntColony(int alpha, int beta, double rho, double q, int numCities, int numAnts, int maxTime)
        {
            Alpha = alpha;
            Beta = beta;
            Rho = rho;
            Q = q;
            NumCities = numCities;
            NumAnts = numAnts;
            MaxTime = maxTime;
        }

        // МЕТОДЫ

        public void Execute(int[][] dists)
        {
            try
            {
                if (dists == null)
                {
                    //Тестовая штука
                    _dists = new[]
                    {
                    new[] { NoPath, 3,      NoPath, NoPath, 1,      NoPath  }, //noPath
                    new[] { 3,      NoPath, 8,      NoPath, NoPath, 3       }, //1
                    new[] { NoPath, 3,      NoPath, 1,      NoPath, 1       }, //2
                    new[] { NoPath, NoPath, 8,      NoPath, 1,      NoPath  }, //3
                    new[] { 3,      NoPath, NoPath, 3,      NoPath, NoPath  }, //4
                    new[] { 3,      3,      3,      5,      4,      NoPath  }  //5
                    };
                }
                else
                {
                    for (var i = 0; i < 6; i++)
                    {
                        for (var j = 0; j < 6; j++)
                        {
                            if (dists[i][j] == 0) dists[i][j] = NoPath;
                        }
                    }
                    _dists = dists;

                }


                // Инициализируем муравьишек и все остальное
                _ants = InitAnts();
                _bestTrail = BestTrail();
                _pheromones = InitPheromones();

                var bestLength = Length(_bestTrail, _dists);
                var time = 0;

                // Запускаем симуляцию
                while (time < MaxTime)
                {
                    UpdateAnts();
                    UpdatePheromones();
                    var currBestTrail = BestTrail();
                    var currBestLength = Length(currBestTrail, _dists);
                    if (currBestLength < bestLength)
                    {
                        bestLength = currBestLength;
                        _bestTrail = currBestTrail;
                    }
                    time++;
                }
                //LINQ expression
                var bestTrailString = _bestTrail.Aggregate("", (current, item) => current + ((char)('A'+int.Parse(item.ToString())) + " -> "));
                MessageBox.Show($"Муравьиный алгоритм успешно закончил работу. \nЛучший путь: {bestTrailString}\nДлина пути: {bestLength}");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Во время работы муравьиного алгоритма выскочила ошибка. Текст ошибки:\n{ex}");
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private int[][] InitAnts()
        {
            var ants = new int[NumAnts][];
            for (var k = 0; k <= NumAnts - 1; k++)
            {
                var start = Random.Next(0, NumCities);
                ants[k] = RandomTrail(start, NumCities);
            }
            return ants;
        }

        private int[] RandomTrail(int start, int numCities)
        {
            int[] trail = new int[numCities];

            for (int i = 0; i <= numCities - 1; i++)
            {
                trail[i] = i;
            }

            for (int i = 0; i <= numCities - 1; i++)
            {
                int r = Random.Next(i, numCities);
                int tmp = trail[r];
                trail[r] = trail[i];
                trail[i] = tmp;
            }

            int idx = IndexOfTarget(trail, start);
            int temp = trail[0];
            trail[0] = trail[idx];
            trail[idx] = temp;

            return trail;
        }

        private static int IndexOfTarget(int[] trail, int target)
        {
            for (int i = 0; i <= trail.Length - 1; i++)
            {
                if (trail[i] == target)
                {
                    return i;
                }
            }
            throw new Exception("Target not found in IndexOfTarget");
        }

        private static double Length(int[] trail, int[][] dists)
        {
            double result = 0.0;
            for (int i = 0; i <= trail.Length - 2; i++)
            {
                result += Distance(trail[i], trail[i + 1], dists);
            }
            return result;
        }

        //Находит лучший путь
        private int[] BestTrail()
        {
            double bestLength = Length(_ants[0], _dists);
            int idxBestLength = 0;
            for (int k = 1; k <= _ants.Length - 1; k++)
            {
                double len = Length(_ants[k], _dists);
                if (len < bestLength)
                {
                    bestLength = len;
                    idxBestLength = k;
                }
            }
            int numCities = _ants[0].Length;
            int[] bestTrailRenamed = new int[numCities];
            _ants[idxBestLength].CopyTo(bestTrailRenamed, 0);
            return bestTrailRenamed;
        }

        private double[][] InitPheromones()
        {
            double[][] pheromones = new double[NumCities][];
            for (int i = 0; i <= NumCities - 1; i++)
            {
                pheromones[i] = new double[NumCities];
            }
            for (int i = 0; i <= pheromones.Length - 1; i++)
            {
                for (int j = 0; j <= pheromones[i].Length - 1; j++)
                {
                    pheromones[i][j] = 0.01;
                }
            }
            return pheromones;
        }

        private void UpdateAnts()
        {
            int numCities = _pheromones.Length;
            for (int k = 0; k <= _ants.Length - 1; k++)
            {
                int start = Random.Next(0, numCities);
                int[] newTrail = BuildTrail(k, start, _pheromones, _dists);
                _ants[k] = newTrail;
            }
        }

        private int[] BuildTrail(int k, int start, double[][] pheromones, int[][] dists)
        {
            int numCities = pheromones.Length;
            int[] trail = new int[numCities];
            bool[] visited = new bool[numCities];
            trail[0] = start;
            visited[start] = true;
            for (int i = 0; i <= numCities - 2; i++)
            {
                int cityX = trail[i];
                int next = NextCity(k, cityX, visited, pheromones, dists);
                trail[i + 1] = next;
                visited[next] = true;
            }
            return trail;
        }

        private int NextCity(int k, int cityX, bool[] visited, double[][] pheromones, int[][] dists)
        {
            double[] probs = MoveProbs(cityX, visited, pheromones, dists);
            double[] cumul = new double[probs.Length + 1];

            for (int i = 0; i <= probs.Length - 1; i++)
            {
                cumul[i + 1] = cumul[i] + probs[i];
            }

            double p = Random.NextDouble();

            for (int i = 0; i <= cumul.Length - 2; i++)
            {
                if (p >= cumul[i] && p < cumul[i + 1])
                {
                    return i;
                }
            }
            throw new Exception("Failure to return valid city in NextCity");
        }

        private double[] MoveProbs(int cityX, bool[] visited, double[][] pheromones, int[][] dists)
        {
            int numCities = pheromones.Length;
            double[] taueta = new double[numCities];
            double sum = 0.0;
            for (int i = 0; i <= taueta.Length - 1; i++)
            {
                if (i == cityX)
                {
                    taueta[i] = 0.0;
                }
                else if (visited[i])
                {
                    taueta[i] = 0.0;
                }
                else
                {
                    taueta[i] = Math.Pow(pheromones[cityX][i], Alpha) * Math.Pow((1.0 / Distance(cityX, i, dists)), Beta);
                    if (taueta[i] < 0.0001)
                    {
                        taueta[i] = 0.0001;
                    }
                    else if (taueta[i] > (double.MaxValue / (numCities * 100)))
                    {
                        taueta[i] = double.MaxValue / (numCities * 100);
                    }
                }
                sum += taueta[i];
            }

            double[] probs = new double[numCities];
            for (int i = 0; i <= probs.Length - 1; i++)
            {
                probs[i] = taueta[i] / sum;
            }
            return probs;
        }

        private void UpdatePheromones()
        {
            for (int i = 0; i <= _pheromones.Length - 1; i++)
            {
                for (int j = i + 1; j <= _pheromones[i].Length - 1; j++)
                {
                    for (int k = 0; k <= _ants.Length - 1; k++)
                    {
                        var length = Length(_ants[k], _dists);
                        double decrease = (1.0 - Rho) * _pheromones[i][j];
                        double increase = 0.0;
                        if (EdgeInTrail(i, j, _ants[k]))
                            increase = (Q / length);
                        
                        _pheromones[i][j] = decrease + increase;

                        if (_pheromones[i][j] < 0.0001)
                        {
                            _pheromones[i][j] = 0.0001;
                        }
                        else if (_pheromones[i][j] > 100000.0)
                        {
                            _pheromones[i][j] = 100000.0;
                        }

                        _pheromones[j][i] = _pheromones[i][j];
                    }
                }
            }
        }

        // Определяет рядом ли cityX и cityY в trail
        private bool EdgeInTrail(int cityX, int cityY, int[] trail)
        {
            int lastIndex = trail.Length - 1;
            int idx = IndexOfTarget(trail, cityX);

            if (idx == 0 && trail[1] == cityY)
            {
                return true;
            }
            else if (idx == 0 && trail[lastIndex] == cityY)
            {
                return true;
            }
            else if (idx == 0)
            {
                return false;
            }
            else if (idx == lastIndex && trail[lastIndex - 1] == cityY)
            {
                return true;
            }
            else if (idx == lastIndex && trail[0] == cityY)
            {
                return true;
            }
            else if (idx == lastIndex)
            {
                return false;
            }
            else if (trail[idx - 1] == cityY)
            {
                return true;
            }
            else if (trail[idx + 1] == cityY)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static double Distance(int cityX, int cityY, int[][] dists)
        {
            return dists[cityX][cityY];
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace Part2
{
    /// <summary>
    /// Класс для вывода в таблицу - отражает строку
    /// </summary>
    public class Export
    {
        public double Y1 { get; set; }
        public double Y2 { get; set; }
        public double Y3 { get; set; }
        public double Y4 { get; set; }
        public double Y5 { get; set; }
        public double Y6 { get; set; }
        public double Y7 { get; set; }
        public double Cmin1 { get; set; }
        public double Cmin2 { get; set; }
        public double DeltaCMin { get; set; }

        /// <summary>
        /// Конструктор по заданым результатам
        /// </summary>
        /// <param name="y1"></param>
        /// <param name="y2"></param>
        /// <param name="y3"></param>
        /// <param name="y4"></param>
        /// <param name="y5"></param>
        /// <param name="y6"></param>
        /// <param name="y7"></param>
        public Export(double y1, double y2, double y3, double y4, double y5, double y6, double y7)
        {
            Y1 = y1;
            Y2 = y2;
            Y3 = y3;
            Y4 = y4;
            Y5 = y5;
            Y6 = y6;
            Y7 = y7;
        }

        public Export()
        {
            Y1 = 0;
            Y2 = 0;
            Y3 = 0;
            Y4 = 0;
            Y5 = 0;
            Y6 = 0;
            Y7 = 0;
        }

        public Vector<double> GetVector()
        {
            var res = new DenseVector(new []{Y1,Y2,Y3,Y4,Y5,Y6,Y7});
            return res;
        }
    }
}

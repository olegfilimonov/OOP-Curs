﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using Application = Microsoft.Office.Interop.Excel.Application;

namespace Part2
{
    public partial class Form1 : Form
    {
        private const int M = 7, N = 6;
        public List<Export> Exports;
        private Matrix<double> matrix;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Exports = new List<Export>(N);
            for (var i = 0; i < N; i++)
            {
                Exports.Add(new Export());
            }

            foreach (var export in Exports)
            {
                bindingSource1.Add(export);
            }
        }

        private void ToCsV()
        {
            if (dataGridView1.Rows.Count <= 0) return;
            var xcelApp = new Application();
            xcelApp.Application.Workbooks.Add(Type.Missing);

            for (var i = 1; i < dataGridView1.Columns.Count + 1; i++)
            {
                xcelApp.Cells[1, i] = dataGridView1.Columns[i - 1].HeaderText;
            }

            for (var i = 0; i < dataGridView1.Rows.Count; i++)
            {
                for (var j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    xcelApp.Cells[i + 2, j + 1] = dataGridView1.Rows[i].Cells[j].Value.ToString().Replace(',', '.');
                }
            }
            xcelApp.Columns.AutoFit();
            xcelApp.Visible = true;
        }

        private void bindingSource1_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ToCsV();
        }

        private bool[] used;
        private int[] mt;

        bool kuhn(int v)
        {
            if (used[v]) return false;
            used[v] = true;
            foreach (var to in matrix.Row(v).Where(to => mt[(int)to] == -1 || kuhn(mt[(int)to])))
            {
                mt[(int)to] = v;
                return true;
            }
            return false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Cоздаем матрицу 6х6
            matrix = new DenseMatrix(N,M);
            for (var i = 0; i < N; i++)
            {
                var temp = Exports[i].GetVector();
                for (var j = 0; j < M; j++)
                {
                    matrix[i, j] = temp[j];
                }
            }
            MessageBox.Show($"Делаем Крускала следующей матрицы\n{matrix}");
            Vector<double> cVector1 = new DenseVector(M);
            Vector<double> cVector2 = new DenseVector(M);
            
            for (var i = 0; i < N; i++)
            {
                var temp = matrix.Column(i);
                var min1 = temp.Minimum();
                temp[temp.MinimumIndex()] = temp.Maximum();
                var min2 = temp.Minimum();
                cVector1[i] = min1;
                cVector2[i] = min2;
            }

            var diffVector = cVector2 - cVector1;
            for (var i = 0; i < N; i++)
            {
                Exports[i].Cmin1 = cVector1[i];
                Exports[i].Cmin2 = cVector2[i];
                Exports[i].DeltaCMin = diffVector[i];
            }

            for (int i = 0; i < mt.Length; i++)
            {
                mt[i] = -1;
            }
            

            dataGridView1.Update();
        }
    }
}

﻿namespace Part2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.y1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.y2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.y3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.y4DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.y5DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.y6DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.y7DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmin1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmin2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deltaCMinDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.y1DataGridViewTextBoxColumn,
            this.y2DataGridViewTextBoxColumn,
            this.y3DataGridViewTextBoxColumn,
            this.y4DataGridViewTextBoxColumn,
            this.y5DataGridViewTextBoxColumn,
            this.y6DataGridViewTextBoxColumn,
            this.y7DataGridViewTextBoxColumn,
            this.cmin1DataGridViewTextBoxColumn,
            this.cmin2DataGridViewTextBoxColumn,
            this.deltaCMinDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.bindingSource1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(13, 13);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridView1.RowTemplate.ErrorText = "???";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.Size = new System.Drawing.Size(1007, 188);
            this.dataGridView1.TabIndex = 0;
            // 
            // y1DataGridViewTextBoxColumn
            // 
            this.y1DataGridViewTextBoxColumn.DataPropertyName = "Y1";
            this.y1DataGridViewTextBoxColumn.HeaderText = "Y1";
            this.y1DataGridViewTextBoxColumn.Name = "y1DataGridViewTextBoxColumn";
            // 
            // y2DataGridViewTextBoxColumn
            // 
            this.y2DataGridViewTextBoxColumn.DataPropertyName = "Y2";
            this.y2DataGridViewTextBoxColumn.HeaderText = "Y2";
            this.y2DataGridViewTextBoxColumn.Name = "y2DataGridViewTextBoxColumn";
            // 
            // y3DataGridViewTextBoxColumn
            // 
            this.y3DataGridViewTextBoxColumn.DataPropertyName = "Y3";
            this.y3DataGridViewTextBoxColumn.HeaderText = "Y3";
            this.y3DataGridViewTextBoxColumn.Name = "y3DataGridViewTextBoxColumn";
            // 
            // y4DataGridViewTextBoxColumn
            // 
            this.y4DataGridViewTextBoxColumn.DataPropertyName = "Y4";
            this.y4DataGridViewTextBoxColumn.HeaderText = "Y4";
            this.y4DataGridViewTextBoxColumn.Name = "y4DataGridViewTextBoxColumn";
            // 
            // y5DataGridViewTextBoxColumn
            // 
            this.y5DataGridViewTextBoxColumn.DataPropertyName = "Y5";
            this.y5DataGridViewTextBoxColumn.HeaderText = "Y5";
            this.y5DataGridViewTextBoxColumn.Name = "y5DataGridViewTextBoxColumn";
            // 
            // y6DataGridViewTextBoxColumn
            // 
            this.y6DataGridViewTextBoxColumn.DataPropertyName = "Y6";
            this.y6DataGridViewTextBoxColumn.HeaderText = "Y6";
            this.y6DataGridViewTextBoxColumn.Name = "y6DataGridViewTextBoxColumn";
            // 
            // y7DataGridViewTextBoxColumn
            // 
            this.y7DataGridViewTextBoxColumn.DataPropertyName = "Y7";
            this.y7DataGridViewTextBoxColumn.HeaderText = "Y7";
            this.y7DataGridViewTextBoxColumn.Name = "y7DataGridViewTextBoxColumn";
            // 
            // cmin1DataGridViewTextBoxColumn
            // 
            this.cmin1DataGridViewTextBoxColumn.DataPropertyName = "Cmin1";
            this.cmin1DataGridViewTextBoxColumn.HeaderText = "Cmin1";
            this.cmin1DataGridViewTextBoxColumn.Name = "cmin1DataGridViewTextBoxColumn";
            this.cmin1DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cmin2DataGridViewTextBoxColumn
            // 
            this.cmin2DataGridViewTextBoxColumn.DataPropertyName = "Cmin2";
            this.cmin2DataGridViewTextBoxColumn.HeaderText = "Cmin2";
            this.cmin2DataGridViewTextBoxColumn.Name = "cmin2DataGridViewTextBoxColumn";
            this.cmin2DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // deltaCMinDataGridViewTextBoxColumn
            // 
            this.deltaCMinDataGridViewTextBoxColumn.DataPropertyName = "DeltaCMin";
            this.deltaCMinDataGridViewTextBoxColumn.HeaderText = "DeltaCMin";
            this.deltaCMinDataGridViewTextBoxColumn.Name = "deltaCMinDataGridViewTextBoxColumn";
            this.deltaCMinDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(Part2.Export);
            this.bindingSource1.CurrentChanged += new System.EventHandler(this.bindingSource1_CurrentChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 207);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(635, 54);
            this.button1.TabIndex = 1;
            this.button1.Text = "ПРОИЗВЕСТИ РАСЧЕТЫ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(654, 207);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(366, 56);
            this.button2.TabIndex = 2;
            this.button2.Text = "ЭКСПОРТИРОВАТЬ";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1032, 275);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        public System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.DataGridViewTextBoxColumn y1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn y2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn y3DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn y4DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn y5DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn y6DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn y7DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cmin1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cmin2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deltaCMinDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}


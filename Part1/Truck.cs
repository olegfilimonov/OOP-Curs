﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Curs
{
    class Truck:Car
    {
        private readonly ulong _liftingCapasity;

        public Truck(Wheel wheel, ulong price, string brand, ulong liftingCapasity) 
            : base(wheel, price, brand)
        {
            _liftingCapasity = liftingCapasity;
        }

        public override string ToString()
        {
            return $"Грузовик, грузоподъемность:{_liftingCapasity}, радиус колеса:{Wheel.Radius}, цена:{Price}, бренд:{Brand}";
        }

        ~Truck()
        {

        }
    }
}

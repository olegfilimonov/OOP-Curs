﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Curs
{
    class Wheel
    {
        public int Radius { get; }

        public Wheel(int radius)
        {
            Radius = radius;
        }
    }
}

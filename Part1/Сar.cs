﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Curs
{
    class Car
    {
        protected Wheel Wheel;
        protected ulong Price;
        protected string Brand;

        public Car(Wheel wheel, ulong price, string brand)
        {
            Wheel = wheel;
            Price = price;
            Brand = brand;
        }

        ~Car()
        {
            
        }
    }
}

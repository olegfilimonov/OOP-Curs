﻿namespace Part5
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.methodList5 = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.iterBox5 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.answerBox5 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.startingBox5 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.epsBox5 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.formulaBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(150, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(24, 23);
            this.button4.TabIndex = 22;
            this.button4.Text = "3";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(120, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(24, 23);
            this.button3.TabIndex = 21;
            this.button3.Text = "2";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(90, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(24, 23);
            this.button2.TabIndex = 20;
            this.button2.Text = "1";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // methodList5
            // 
            this.methodList5.Enabled = false;
            this.methodList5.FormattingEnabled = true;
            this.methodList5.Location = new System.Drawing.Point(15, 101);
            this.methodList5.Name = "methodList5";
            this.methodList5.Size = new System.Drawing.Size(335, 24);
            this.methodList5.TabIndex = 19;
            this.methodList5.SelectedIndexChanged += new System.EventHandler(this.methodList5_SelectedIndexChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(12, 54);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(192, 17);
            this.label29.TabIndex = 18;
            this.label29.Text = "Обнаружено переменных: 0";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(12, 83);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(54, 17);
            this.label30.TabIndex = 12;
            this.label30.Text = "Метод:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(844, 9);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(77, 17);
            this.label34.TabIndex = 13;
            this.label34.Text = "Итераций:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(698, 9);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(52, 17);
            this.label33.TabIndex = 14;
            this.label33.Text = "Ответ:";
            // 
            // iterBox5
            // 
            this.iterBox5.BackColor = System.Drawing.SystemColors.Window;
            this.iterBox5.Location = new System.Drawing.Point(847, 29);
            this.iterBox5.Name = "iterBox5";
            this.iterBox5.ReadOnly = true;
            this.iterBox5.Size = new System.Drawing.Size(76, 22);
            this.iterBox5.TabIndex = 6;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(364, 83);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(136, 17);
            this.label32.TabIndex = 15;
            this.label32.Text = "Начальный вектор:";
            // 
            // answerBox5
            // 
            this.answerBox5.BackColor = System.Drawing.SystemColors.Window;
            this.answerBox5.Location = new System.Drawing.Point(606, 29);
            this.answerBox5.Multiline = true;
            this.answerBox5.Name = "answerBox5";
            this.answerBox5.ReadOnly = true;
            this.answerBox5.Size = new System.Drawing.Size(232, 96);
            this.answerBox5.TabIndex = 9;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(364, 9);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(74, 17);
            this.label31.TabIndex = 16;
            this.label31.Text = "Точность:";
            // 
            // startingBox5
            // 
            this.startingBox5.Enabled = false;
            this.startingBox5.Location = new System.Drawing.Point(367, 103);
            this.startingBox5.Name = "startingBox5";
            this.startingBox5.Size = new System.Drawing.Size(129, 22);
            this.startingBox5.TabIndex = 8;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(12, 9);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(71, 17);
            this.label28.TabIndex = 17;
            this.label28.Text = "Функция:";
            // 
            // epsBox5
            // 
            this.epsBox5.Enabled = false;
            this.epsBox5.Location = new System.Drawing.Point(367, 29);
            this.epsBox5.Name = "epsBox5";
            this.epsBox5.Size = new System.Drawing.Size(129, 22);
            this.epsBox5.TabIndex = 7;
            this.epsBox5.Text = "1e-5";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(265, 29);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(85, 42);
            this.button1.TabIndex = 11;
            this.button1.Text = "PARSE";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // formulaBox
            // 
            this.formulaBox.Location = new System.Drawing.Point(15, 29);
            this.formulaBox.Name = "formulaBox";
            this.formulaBox.Size = new System.Drawing.Size(244, 22);
            this.formulaBox.TabIndex = 10;
            this.formulaBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 153);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.methodList5);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.iterBox5);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.answerBox5);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.startingBox5);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.epsBox5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.formulaBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox methodList5;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox iterBox5;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox answerBox5;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox startingBox5;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox epsBox5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox formulaBox;
    }
}


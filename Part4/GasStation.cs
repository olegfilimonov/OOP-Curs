﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Part4
{
    class GasStation
    {
        private readonly List<Fuel> _fuels; //Список типов топлива
        public ulong GrossProfit { get; private set; }
        private readonly Random _random = new Random(DateTime.Now.Millisecond);

        public ulong Super98Amount => _fuels[0].Amount;
        public ulong Regular92Amount => _fuels[1].Amount;
        public ulong Normal80Amount => _fuels[2].Amount;


        /// <summary>
        /// Конструктор заправки
        /// </summary>
        /// <param name="super98Amount">количество 98</param>
        /// <param name="regular92Amount">количество 92</param>
        /// <param name="normal80Amount">количество 80</param>
        public GasStation(ulong super98Amount, ulong regular92Amount, ulong normal80Amount)
        {
            _fuels = new List<Fuel>(3);

            //Создаем
            var super98 = new Fuel(super98Amount, FuelType.Super98);
            var regular92 = new Fuel(regular92Amount, FuelType.Regular92);
            var normal80 = new Fuel(normal80Amount, FuelType.Normal80);

            //Добавляем в список
            _fuels.Add(super98);
            _fuels.Add(regular92);
            _fuels.Add(normal80);
        }

        public bool Buy(ulong amount)
        {

            var result = _random.NextDouble();
            FuelType chosenFuelType;

            //Находим случайно выбранный тип топлива
            if (result > _fuels[0].Probability + _fuels[1].Probability) chosenFuelType = FuelType.Normal80;
            else if (result > _fuels[0].Probability) chosenFuelType = FuelType.Regular92;
            else chosenFuelType = FuelType.Super98;

            //Выбираем тип топлива
            var chosenFuel = _fuels[(int)chosenFuelType];

            //Производим покупку
            chosenFuel.Remove(amount);
            GrossProfit += amount * (chosenFuel.Price - chosenFuel.CostPrice);

            return true;

        }

        public override string ToString()
        {
            return $"Статус автозаправки:\nВаловая прибыль: {GrossProfit}\n{_fuels[0]}\n{_fuels[1]}\n{_fuels[2]}";
        }
    }
}

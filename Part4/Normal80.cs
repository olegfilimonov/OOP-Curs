﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part4
{
    class Normal80:Fuel
    {
        private const double DefaultNormal80Probability = 0.4;
        private const ulong DefaultNormal80CostPrice = 23;
        private const ulong DefaultNormal80Price = 31;

        public Normal80(ulong amount) : base(amount, DefaultNormal80Probability, DefaultNormal80CostPrice,  DefaultNormal80Price,"Нормаль-80")
        {
        }

        public Normal80(ulong amount, double probability) : base(amount, probability,  DefaultNormal80CostPrice, DefaultNormal80CostPrice, "Нормаль-80")
        {
        }

        public Normal80(ulong amount, double probability, ulong costPrice, ulong price) : base(amount, probability, costPrice, price, "Нормаль-80")
        {
        }
    }
}

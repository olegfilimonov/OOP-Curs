﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using FPlotLibrary;
using OxyPlot;
using OxyPlot.Extensions;

namespace Part4
{

    public partial class MainForm : Form
    {
        private GasStation _gasStation;
        DataItem incomeData = new DataItem();
        DataItem amountData1 = new DataItem();
        DataItem amountData2 = new DataItem();
        DataItem amountData3 = new DataItem();

        private List<string> income = new List<string>();
        private List<string> amount1 = new List<string>();
        private List<string> amount2 = new List<string>();
        private List<string> amount3 = new List<string>();

        private List<string> xAxis = new List<string>();
        private List<string> dAxis = new List<string>();
        private List<string> yAxis = new List<string>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void UpdateLog()
        {
            var report = _gasStation.ToString();
            report = report.Replace("\n", Environment.NewLine);
            logTextBox.Text = report;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                income.Clear();
                amount1.Clear();
                amount2.Clear();
                amount3.Clear();

                
                var super98Amount = ulong.Parse(super98TextBox.Text);
                var regular92Amount = ulong.Parse(regular92TextBox.Text);
                var normal80Amount = ulong.Parse(Normal80TextBox.Text);

                _gasStation = new GasStation(super98Amount, regular92Amount, normal80Amount);
                UpdateLog();
                

                incomeData.Color = Color.Blue;
                incomeData.lineWidth = 3;
                incomeData.lines = true;

                amountData1.Color = Color.Red;
                amountData2.Color = Color.LawnGreen;
                amountData3.Color = Color.MediumPurple;
                amountData1.lines = true;
                amountData2.lines = true;
                amountData3.lines = true;

                amountData1.lineWidth = 3;
                amountData2.lineWidth = 3;
                amountData3.lineWidth = 3;



                graphControl1.Model.Items.Add(incomeData);
                graphControl1.Model.Items.Add(amountData1);
                graphControl1.Model.Items.Add(amountData2);
                graphControl1.Model.Items.Add(amountData3);



                graphControl1.Invalidate();

                timer1.Interval = int.Parse(textBox1.Text);
                timer1.Start();
            }
            catch (Exception exception)
            {
                MessageBox.Show($"Кажется, введены неверные параметры в начальные условия\nТекст ошибки:{exception.Message}");
            }

        }




        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                _gasStation.Buy(ulong.Parse(textBox2.Text));
                UpdateLog();
                var num = xAxis.Count;
                if (graphControl1.x1 < num) graphControl1.x1 = num + 5;
                xAxis.Add(num.ToString());
                yAxis.Add("10");
                income.Add(_gasStation.GrossProfit.ToString());
                amount1.Add(_gasStation.Normal80Amount.ToString());
                amount2.Add(_gasStation.Regular92Amount.ToString());
                amount3.Add(_gasStation.Super98Amount.ToString());
                incomeData.Parse(xAxis.ToArray(), income.ToArray(),  dAxis.ToArray(), yAxis.ToArray());
                amountData1.Parse(xAxis.ToArray(),amount1.ToArray(),  dAxis.ToArray(), yAxis.ToArray());
                amountData2.Parse(xAxis.ToArray(),amount2.ToArray(),  dAxis.ToArray(), yAxis.ToArray());
                amountData3.Parse(xAxis.ToArray(),amount3.ToArray(), dAxis.ToArray(), yAxis.ToArray());
                graphControl1.Invalidate();
            }
            catch (Exception exception)
            {
                timer1.Stop();
                MessageBox.Show($"Модель прекратила работу\nТекст ошибки:{exception.Message}");
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.CodeDom;

namespace Part4
{
    public class Fuel
    {
        private ulong _amount;
        private double _probability;
        private ulong _costPrice;
        private ulong _price;
        private readonly FuelType _type;
        
        public Fuel(ulong amount, double probability, ulong costPrice, ulong price, FuelType type)
        {
            _amount = amount;
            Probability = probability;
            CostPrice = costPrice;
            Price = price;
            _type = type;
        }

        public Fuel(ulong amount, FuelType type)
            : this(amount, GetDefaultProbability(type),  GetDefaultCostPrice(type),GetDefaultPrice(type), type)
        {
        }

        private static double GetDefaultProbability(FuelType type)
        {
            switch (type)
            {
                case FuelType.Super98:
                    return 0.4;
                case FuelType.Regular92:
                    return 0.3;
                case FuelType.Normal80:
                    return 0.3;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, "Ошибка в задании вероятности");
            }
        }

        private static ulong GetDefaultPrice(FuelType type)
        {
            switch (type)
            {
                case FuelType.Super98:
                    return 42;
                case FuelType.Regular92:
                    return 35;
                case FuelType.Normal80:
                    return 31;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, "Ошибка в задании цены");
            }
        }

        private static ulong GetDefaultCostPrice(FuelType type)
        {
            switch (type)
            {
                case FuelType.Super98:
                    return 35;
                case FuelType.Regular92:
                    return 30;
                case FuelType.Normal80:
                    return 25;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, "Ошибка в задании себестоимости");
            }
        }

        public override string ToString()
        {
            return
                $"{_type}:  \tКол-во: {_amount}, \tЦена: {_price}, \tВероятность: {_probability}, \tСебестоимость: {_costPrice}, \tЦена: {_price}";
        }

        public ulong Amount => _amount;

        public void Remove(ulong amount)
        {
            if(amount>_amount) throw new ArgumentOutOfRangeException("Нельзя купить больше, чем есть на складе");
            _amount -= amount;
        }

        public double Probability
        {
            get
            {
                if (_probability > 1 || _probability < 0) throw new ArgumentOutOfRangeException("Ошибка в расчете вероятности");
                return _probability;
            }
            set
            {
                if (value > 1 || value < 0) throw new ArgumentOutOfRangeException("Ошибка в задании вероятности");
                _probability = value;
            }
        }

        public ulong CostPrice
        {
            get { return _costPrice; }

            set { _costPrice = value; }
        }

        public ulong Price
        {
            get { return _price; }

            set { _price = value; }
        }
    }
}